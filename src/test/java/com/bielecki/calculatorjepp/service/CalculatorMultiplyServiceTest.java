package com.bielecki.calculatorjepp.service;

import com.bielecki.calculatorjepp.model.Operand;
import com.bielecki.calculatorjepp.model.Unit;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

public class CalculatorMultiplyServiceTest {

    private CalculatorMultiplyService calculatorMultiplyService;

    Operand bigInt;
    Operand smallInt;
    Operand smallIntFoot;
    Operand negativeInt;
    Operand floatValue;
    Operand nullValue;

    @Before
    public void setup() {
        calculatorMultiplyService = new CalculatorMultiplyService();
        bigInt = new Operand(BigDecimal.valueOf(9999999), Unit.METER);
        smallInt = new Operand(BigDecimal.valueOf(1), Unit.METER);
        smallIntFoot = new Operand(BigDecimal.valueOf(1), Unit.FOOT);
        negativeInt = new Operand(BigDecimal.valueOf(-9999999), Unit.METER);
        floatValue = new Operand(BigDecimal.valueOf(0.0001), Unit.METER);
        nullValue = null;
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNullArgument1() {
        calculatorMultiplyService.calculate(null, bigInt);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNullArgument2() {
        calculatorMultiplyService.calculate(bigInt, null);
    }

    @Test
    public void testCorrectCalculationIntFloat() {
        Assert.assertEquals(new BigDecimal(999.99990).setScale(5, BigDecimal.ROUND_HALF_UP), calculatorMultiplyService.calculate(bigInt, floatValue));
    }

    @Test
    public void testCorrectCalculationDifferentUnits() {
        Assert.assertEquals(new BigDecimal(0.30480).setScale(5, BigDecimal.ROUND_HALF_UP), calculatorMultiplyService.calculate(smallInt, smallIntFoot));
    }
}
