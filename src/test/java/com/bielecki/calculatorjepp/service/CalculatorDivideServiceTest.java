package com.bielecki.calculatorjepp.service;

import com.bielecki.calculatorjepp.model.Operand;
import com.bielecki.calculatorjepp.model.Unit;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

public class CalculatorDivideServiceTest {

    private CalculatorDivideService calculatorDivideService;

    Operand bigInt;
    Operand smallInt;
    Operand smallIntFoot;
    Operand negativeInt;
    Operand floatValue;
    Operand nullValue;

    @Before
    public void setup() {
        calculatorDivideService = new CalculatorDivideService();
        bigInt = new Operand(BigDecimal.valueOf(99999999), Unit.METER);
        smallInt = new Operand(BigDecimal.valueOf(1), Unit.METER);
        smallIntFoot = new Operand(BigDecimal.valueOf(1), Unit.FOOT);
        negativeInt = new Operand(BigDecimal.valueOf(-99999999), Unit.METER);
        floatValue = new Operand(BigDecimal.valueOf(0.0001), Unit.METER);
        nullValue = null;
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNullArgument1() {
        calculatorDivideService.calculate(null, bigInt);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNullArgument2() {
        calculatorDivideService.calculate(bigInt, null);
    }

    @Test
    public void testCorrectCalculationInt() {
        Assert.assertEquals(new BigDecimal(-1).setScale(5, BigDecimal.ROUND_HALF_UP), calculatorDivideService.calculate(bigInt, negativeInt));
    }

    @Test
    public void testCorrectCalculationIntFloat() {
        Assert.assertEquals(new BigDecimal(999999990000.00000).setScale(5, BigDecimal.ROUND_HALF_UP), calculatorDivideService.calculate(bigInt, floatValue));
    }

    @Test
    public void testCorrectCalculationDifferentUnits() {
        Assert.assertEquals(new BigDecimal(3.28084).setScale(5, BigDecimal.ROUND_HALF_UP), calculatorDivideService.calculate(smallInt, smallIntFoot));
    }
}
