package com.bielecki.calculatorjepp.repository;

import com.bielecki.calculatorjepp.model.Expression;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ExpressionsRepository extends JpaRepository<Expression, Long> {
}
