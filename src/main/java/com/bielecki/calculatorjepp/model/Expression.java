package com.bielecki.calculatorjepp.model;

import javax.persistence.*;

@Entity
@Table(name = "expression")
public class Expression {

    @Id
    @GeneratedValue
    private Long id;

    @OneToOne(cascade = CascadeType.ALL, optional = false)
    private Operand arg1;

    @OneToOne(cascade = CascadeType.ALL, optional = false)
    private Operand arg2;

    @Enumerated(EnumType.STRING)
    private Operator operator;

    @OneToOne(cascade = CascadeType.ALL, optional = false)
    private Operand result;


    public Expression() {
        this.arg1 = new Operand();
        this.arg2 = new Operand();
        this.result = new Operand();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Operand getArg1() {
        return arg1;
    }

    public void setArg1(Operand arg1) {
        this.arg1 = arg1;
    }

    public Operand getArg2() {
        return arg2;
    }

    public void setArg2(Operand arg2) {
        this.arg2 = arg2;
    }

    public Operator getOperator() {
        return operator;
    }

    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    public Operand getResult() {
        return result;
    }

    public void setResult(Operand result) {
        this.result = result;
    }
}
