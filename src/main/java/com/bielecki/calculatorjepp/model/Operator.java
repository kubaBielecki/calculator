package com.bielecki.calculatorjepp.model;

import com.bielecki.calculatorjepp.service.*;

public enum Operator {
    ADD("+", new CalculatorAddService()),
    SUBTRACT("-", new CalculatorSubtractService()),
    MULTIPLY("*", new CalculatorMultiplyService()),
    DIVIDE("/", new CalculatorMultiplyService());

    private String label;
    private CalculatorServiceInterface calculator;

    private Operator(String label, CalculatorServiceInterface calculator) {
        this.label = label;
        this.calculator = calculator;
    }

    public String getLabel() {
        return label;
    }

    public CalculatorServiceInterface getCalculator() {
        return calculator;
    }
}
