package com.bielecki.calculatorjepp.model;

public enum Unit {
    METER(1.0),
    FOOT(3.2808);

    private double ratio;

    Unit(double ratio) {
        this.ratio = ratio;
    }

    public double getRatio() {
        return ratio;
    }
}
