package com.bielecki.calculatorjepp.model;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "operand")
public class Operand {

    @Id
    @GeneratedValue
    private Long id;

    private BigDecimal value;

    @Enumerated(EnumType.STRING)
    private Unit unit;

    public Operand() {
        unit = Unit.METER;
    }

    public Operand(BigDecimal value, Unit unit) {
        this.unit = unit;
        this.value = value;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }
}
