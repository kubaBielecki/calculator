package com.bielecki.calculatorjepp.service;

import com.bielecki.calculatorjepp.model.Operand;

import java.math.BigDecimal;

public class CalculatorDivideService implements CalculatorServiceInterface {
    @Override
    public BigDecimal calculate(Operand arg1, Operand arg2) throws IllegalArgumentException {
        if (isArgumentValid(arg1) && isArgumentValid(arg2) && !arg2.getValue().equals(BigDecimal.ZERO)) {
            return (parseToMetersAndSetPrecision(arg1).divide(parseToMetersAndSetPrecision(arg2),BigDecimal.ROUND_HALF_DOWN))
                    .setScale(5, BigDecimal.ROUND_HALF_UP);
        } else {
            throw new IllegalArgumentException("Invalid argument");
        }
    }
}
