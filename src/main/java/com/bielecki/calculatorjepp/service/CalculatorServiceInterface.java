package com.bielecki.calculatorjepp.service;

import com.bielecki.calculatorjepp.model.Operand;


import java.math.BigDecimal;

public interface CalculatorServiceInterface {

    BigDecimal calculate(Operand arg1, Operand arg2) throws IllegalArgumentException;

    default boolean isArgumentValid(Operand operand) {
        return operand != null && operand.getValue() != null;
    }

    default BigDecimal parseToMetersAndSetPrecision(Operand arg) {
        return arg.getValue().setScale(5, BigDecimal.ROUND_HALF_UP)
                .divide(BigDecimal.valueOf(arg.getUnit().getRatio()), BigDecimal.ROUND_HALF_UP);
    }
}
