package com.bielecki.calculatorjepp.service;

import com.bielecki.calculatorjepp.model.Operand;

import java.math.BigDecimal;

public class CalculatorAddService implements CalculatorServiceInterface {
    @Override
    public BigDecimal calculate(Operand arg1, Operand arg2) throws IllegalArgumentException {
        if (isArgumentValid(arg1) && isArgumentValid(arg2)) {
            return (parseToMetersAndSetPrecision(arg1).add(parseToMetersAndSetPrecision(arg2)))
                    .setScale(5, BigDecimal.ROUND_HALF_UP);
        } else {
            throw new IllegalArgumentException("Invalid argument");
        }
    }

}
