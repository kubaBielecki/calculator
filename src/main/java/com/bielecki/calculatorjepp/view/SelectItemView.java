package com.bielecki.calculatorjepp.view;

import com.bielecki.calculatorjepp.model.Operator;
import com.bielecki.calculatorjepp.model.Unit;

import javax.inject.Named;

@Named
public class SelectItemView {

    public Unit[] getUnits() {
        return Unit.values();
    }

    public Operator[] getOperators() {
        return Operator.values();
    }
}
