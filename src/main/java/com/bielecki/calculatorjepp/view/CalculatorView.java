//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.bielecki.calculatorjepp.view;

import com.bielecki.calculatorjepp.model.Expression;
import com.bielecki.calculatorjepp.model.Operand;
import com.bielecki.calculatorjepp.model.Unit;
import com.bielecki.calculatorjepp.repository.ExpressionsRepository;
import com.bielecki.calculatorjepp.service.CalculatorServiceInterface;
import java.math.BigDecimal;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Named;
import org.primefaces.PrimeFaces;
import org.springframework.beans.factory.annotation.Autowired;

@Named
public class CalculatorView {
    @Autowired
    private ExpressionsRepository repository;
    private Expression expression;
    private List<Expression> tableData;

    public CalculatorView() {
    }

    public void calculate() {
        if (this.expression == null) {
            FacesContext.getCurrentInstance().addMessage((String)null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Initialization error"));
        }

        try {
            CalculatorServiceInterface service = this.expression.getOperator().getCalculator();
            this.expression.getResult().setValue(service.calculate(this.expression.getArg1(), this.expression.getArg2()));
            PrimeFaces.current().executeScript("PF('dialog').show();");
        } catch (IllegalArgumentException var2) {
            FacesContext.getCurrentInstance().addMessage((String)null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning!", var2.getMessage()));
        } catch (Exception var3) {
            FacesContext.getCurrentInstance().addMessage((String)null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Unexpected Error: ", var3.getMessage()));
        }

    }

    @PostConstruct
    public void init() {
        this.expression = new Expression();
        this.tableData = this.repository.findAll();
    }

    public void save() {
        this.repository.save(this.expression);
        this.expression = new Expression();
        this.tableData = this.repository.findAll();
        PrimeFaces.current().executeScript("PF('dialog').hide();");
    }

    public void remove(Expression exp) {
        this.repository.delete(exp);
        this.tableData = this.repository.findAll();
    }

    public void recalculateResultListener(ValueChangeEvent event) {
        if (this.expression != null && this.expression.getResult() != null && this.expression.getResult().getValue() != null) {
            Operand result = this.expression.getResult();
            Unit newUnit = (Unit)event.getNewValue();
            BigDecimal ratio = new BigDecimal(Unit.FOOT.getRatio());
            BigDecimal newValue = BigDecimal.ZERO;
            if (Unit.METER == newUnit) {
                newValue = result.getValue().divide(ratio, 5);
            } else if (Unit.FOOT == newUnit) {
                newValue = result.getValue().multiply(ratio);
            }

            result.setValue(newValue.setScale(5, 4));
        }

    }

    public Expression getExpression() {
        return this.expression;
    }

    public void setExpression(Expression expression) {
        this.expression = expression;
    }

    public List<Expression> getTableData() {
        return this.tableData;
    }
}
