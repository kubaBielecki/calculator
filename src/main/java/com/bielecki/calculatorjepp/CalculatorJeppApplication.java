package com.bielecki.calculatorjepp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CalculatorJeppApplication {

	public static void main(String[] args) {
		SpringApplication.run(CalculatorJeppApplication.class, args);
	}
}
